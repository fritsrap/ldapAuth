# ldapAuth
LDAP Authentication bridge

This will start a LDAP Server witch can be used to auth against and bridge the auth to where ever you want (TextFile, Database, SMTP Auth...)

![docImg](/img/diagram.png)

# HowTo Server
Starting the Server is simple:
1. Clone this repo
2. run `npm i`
3. spin up the sample server.js

```
var ldapAuth = require('./ldapAuth');

ldapAuth.init({
  serverPort : 389, //Default 389
	ldapServerPassword : "asdg", //Change password here
	warningCallback : function(warning) {
		console.log("warning",warning);
	},
	errorCallback : function(error) {
		console.log("error",error);
	},
	checkPassword : function(auth, isPasswordCorrectCallback) {
		var userName = auth["userName"];
		var userPassword = auth["userPassword"];

		if(userName=="test" && userPassword=="test") { //Test user+pw combination here
			isPasswordCorrectCallback(true);
		} else {
			isPasswordCorrectCallback(false);
		}
	}
});
```

# HowTo Client
* Authentifizierungsquelle: LDAP-Authentifizierung
* Verzeichnistyp: OpenLDAP //Macht wohl aber keinen großen unterschied was man da einstellt bei einer Authentifizierung aber falls vorhanden -> geht das
* Hostname/IP: 134.103.176.108
* port: 389
* Encryption/SLL: nein
* DN Bind (Ldap Benutzername): cn=auth
* Passwort: Ldap Server passwort
* Bais-DN (Basis für Benutzersuche): ou=user
* Benutzerfilter: (&(uid=%s))
* Benutzer-Objektklasse: uid
* Attribut Benutzername: username //Falls die Anwendung keine Anmeldung per Mail zulässt (z.B. Jira) muss dieses Attribut auf "mail" gesetzt werden!
* Attribut Vorname: prename
* Attribut Nachname: lastname
* Attribut Anzeigename: username
* Attribut Email: mail
* Attribut Benutzer-ID: uid //Id wird anhand der Mailadresse berechnet
* Attribut Gruppe: group //Wird in der Regel nicht für Auth gebraucht ist aber gesetzt auf "student" oder "staff" (Anhand der Email Adresse)



