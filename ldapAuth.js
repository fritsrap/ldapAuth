var ldap = require('ldapjs');

var server = ldap.createServer();

var checkPassword = null;
var errorCallback = null;
var warningCallback = null;
var ldapServerPassword = "asdg";
var serverPort = 389;

module.exports = {
    init: function (options) {
        if (options["ldapServerPassword"]) {
            ldapServerPassword = options["ldapServerPassword"];
        } else {
            console.log("Warning: LdapAuth Serverpassword not defined!");
        }

        if (options["checkPassword"]) {
            checkPassword = options["checkPassword"];
        } else {
            console.log("Error: LdapAuth checkPassword function not defined! Auth will not work!");
        }

        if (options["serverPort"]) {
            serverPort = options["serverPort"];
        }

        if (options["warningCallback"]) {
            warningCallback = options["warningCallback"];
        }

        if (options["errorCallback"]) {
            errorCallback = options["errorCallback"];
        }

        loadServer();
    }
}

function loadServer() {
    var usernamesave = "";
    var grounamesave = "";
    //1. First connection and login to ldap Server
    server.bind('cn=auth', function (req, res, next) {
        if (req.dn.toString() !== 'cn=auth' || req.credentials !== ldapServerPassword) {
            if (warningCallback) {
                warningCallback("Someone with invaild ldapServerPassword wants to login.")
            }
            return next(new ldap.InvalidCredentialsError());
        }

        res.end();
        return next();
    });
    //2. Searching for user (We just save it for now and return a success)
    server.search('ou=user', function (req, res, next) {
        var filter = req["filter"];
        //console.log(JSON.stringify(filter))
        if (!filter || !filter["filters"]) {
            if (errorCallback) {
                errorCallback("not filters at ldap search...!");
            }
            return next(new ldap.InvalidCredentialsError());
        }

        //  var dn = req.dn.toString();
        //  console.log("dn-->", dn);

        var username = "";
        var prename = "";
        var lastname = "";
        var mail = "";
        var group = "student";
        for (var i in filter["filters"]) {
            var currentFilter = filter["filters"][i];
            //      console.log("NEW FILTER!--------")
            //      console.log("type:", currentFilter["type"]);
            //      console.log("attribute", currentFilter["attribute"]);
            //      console.log("value", currentFilter["value"]);
            if (currentFilter["type"] == "equal") {
                if (currentFilter["attribute"] == "username" || currentFilter["attribute"] == "uid" || currentFilter["attribute"] == "mail") {
                    username = currentFilter["value"] ? currentFilter["value"].split("@")[0] : "unknown.unknown";
                    mail = currentFilter["value"] ? currentFilter["value"] : "unknown@unknown.de";
                    if (mail && mail != "" && mail.indexOf("@") !== -1) {
                        prename = username.split(".")[0];
                        lastname = username.split(".").length > 1 ? username.split(".")[1] : "unknown";
                        group = mail.indexOf("student") === -1 ? "staff" : "student";
                        grounamesave = group;
                        usernamesave = mail; //Save this for the bind
                    }
                }
            }
        }

        var obj = {
            dn: 'cn=auth, ou=user',
            status: 0, //Success
            attributes: {
                "cn": "user",
                status: 0,
                "username": username,
                "prename": prename,
                "lastname": lastname,
                "uid": Math.abs(hashCode(username)),
                "group": grounamesave,
                "groupdescription": grounamesave,
                "mail": mail
            }
        }

        //    console.log(obj);
        res.send(obj);
        res.end();
        return next();
    });

    //3. Getting password. Then check user an password combination...
    server.bind('ou=user', function (req, res, next) {
        var dn = req.dn.toString();
        var userPassword = req["credentials"];
        var userName = usernamesave;
        //              console.log("NEW", dn);
        //              console.log("bind", req);
        //              console.log("userName:",userName);
        //              console.log("userPassword:",userPassword);
        if (checkPassword) {
            checkPassword({ "userName": userName, "userPassword": userPassword }, function (isCorrectPw) {
                if (isCorrectPw) {
                    console.log("Vaild LOGIN")
                    return res.end();
                } else {
                    return next(new ldap.InvalidCredentialsError());
                }
            });
        } else {
            return next(new ldap.InvalidCredentialsError());
        }
    });
    server.listen(serverPort, function () {
        console.log('Ldap Auth Server listening at ' + server.url);
    });
}

hashCode = function (s) {
    var h = 0, l = s.length, i = 0;
    if (l > 0)
        while (i < l)
            h = (h << 5) - h + s.charCodeAt(i++) | 0;
    return h;
};
